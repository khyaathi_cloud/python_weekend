Current Sprint


Tuples
    what is tuples
    

A tuple is a sequence of values. The values can be any type, and they are indexed by integers, so in that respect tuples are a lot like lists. The important difference is that tuples are immutable.

Examples:

In [54]: a = ('a','b','c','d','e')

In [55]: type(a)
Out[55]: tuple

In [56]: a = 'a','b','c','d','e'

In [57]: type(a)
Out[57]: tuple
Syntax -- tuple = ('values','val2','val3',val4,....)
Accessing a tuple
l
Out[285]: [(3883, 3993, 'djdj'), ('jjk', 99292)]

In [286]: l[0]
Out[286]: (3883, 3993, 'djdj')

In [287]: a=l[0]

In [288]: a
Out[288]: (3883, 3993, 'djdj')

In [289]: a[0]
Out[289]: 3883

In [290]: l
Out[290]: [(3883, 3993, 'djdj'), ('jjk', 99292)]

In [291]: l[0][0]
Out[291]: 3883

In [292]: u take list of 5 tuples of differnt value acces 4th tuple 4element
  File "<ipython-input-292-07422b2f1d00>", line 1
    u take list of 5 tuples of differnt value acces 4th tuple 4element
         ^
SyntaxError: invalid syntax


In [293]: l[0][1]
Out[293]: 3993

In [294]: tuple2=('chetna','sandhya','prasad','joseph')

In [295]: tuple2
Out[295]: ('chetna', 'sandhya', 'prasad', 'joseph')

In [296]: tuple2[2][3]
Out[296]: 's'

In [297]: tuple2[2][-1]
Out[297]: 'd'

In [298]: tuple2[3][-1]
Out[298]: 'h'

In [299]: tuple2[-1][-1]
Out[299]: 'h'

In [300]:  l[0]=('ss','sssshgh')

In [301]: l[0]
Out[301]: ('ss', 'sssshgh')

In [302]: l('ss')=l('hghghg')
  File "<ipython-input-302-e855fadb8dd1>", line 1
    l('ss')=l('hghghg')
SyntaxError: can't assign to function call


In [303]: (a,d,f)=(12,13,14)

In [304]: d
Out[304]: 13

In [305]: f
Out[305]: 14


    Tuple is immutable
    Tuple Assignment
        unpack
    Comparing tuples
    Lists and Tuples
     (a,d,f)=(12,13,14)

In [304]: d
Out[304]: 13

In [305]: f
Out[305]: 14

In [306]: test = "hello@test3'
  File "<ipython-input-306-1957e1988a79>", line 1
    test = "hello@test3'
                       ^
SyntaxError: EOL while scanning string literal


In [307]: test = "hello@test3"

In [308]: type(test)
Out[308]: str

In [309]: test.split('@')
Out[309]: ['hello', 'test3']

In [310]: (hi,what)=test.split('@')

In [311]: hi
Out[311]: 'hello'

In [312]: what
Out[312]: 'test3'

In [313]: test
Out[313]: 'hello@test3'

In [314]: test.split('@')
Out[314]: ['hello', 'test3']

In [315]: (hi,what)=test.split('@')

In [316]: hi
Out[316]: 'hello'

In [317]: what
Out[317]: 'test3'

In [318]: type(test)
Out[318]: str

In [319]: test2 = 'hello hi'

In [320]: test2
Out[320]: 'hello hi'

In [321]: test2.split()
Out[321]: ['hello', 'hi']

In [322]: url = "www.google.com/en/dell"

In [323]: type(url)
Out[323]: str

In [324]: url.split()
Out[324]: ['www.google.com/en/dell']

In [325]: url.split('/')
Out[325]: ['www.google.com', 'en', 'dell']

In [326]: url.split('/')[1]
Out[326]: 'en'

In [327]: (1,2,40) > (20,3,1)
Out[327]: False

In [328]: (1,2,40) > (0,3,1)
Out[328]: True

In [329]: (1,2,40) < (0,3,1)
Out[329]: False

In [330]: (1,1,3) > (1,1,20)
Out[330]: False

In [331]: (0,0,45) == (0,0,9)
Out[331]: False

In [332]: a='test'

In [333]: a
Out[333]: 'test'

In [334]: l=[1,2,3,4]

In [335]: l
Out[335]: [1, 2, 3, 4]

In [336]: zip(a,l)
Out[336]: [('t', 1), ('e', 2), ('s', 3), ('t', 4)]

In [337]: zip(l,a)
Out[337]: [(1, 't'), (2, 'e'), (3, 's'), (4, 't')]
In [344]: zip(l,a)
Out[344]: [('t', 't')]

In [345]: l=[1,2,3,4]

In [346]: l
Out[346]: [1, 2, 3, 4]

In [347]: t='test'

In [348]: zip(l,t)
Out[348]: [(1, 't'), (2, 'e'), (3, 's'), (4, 't')]

In [349]: for i in zip(l,t):
     ...:     print i
     ...:     
(1, 't')
(2, 'e')
(3, 's')
(4, 't')

In [350]: for i,j in zip(l,t):
     ...:     print i,j
     ...:     
     ...:     
1 t
2 e
3 s
4 t

In [351]: l
Out[351]: [1, 2, 3, 4]

In [352]: for i in l:
     ...:     print i
     ...:     
1
2
3
4

In [353]: t
Out[353]: 'test'

In [354]: for i in t:
     ...:     print i
     ...:     
t
e
s
t

In [355]: zip(l,t)
Out[355]: [(1, 't'), (2, 'e'), (3, 's'), (4, 't')]

In [356]: for i,j in zip(l,t):
     ...:     print i,j
     ...:     
1 t
2 e
3 s
4 t

In [357]: l
Out[357]: [1, 2, 3, 4]

In [358]: for k in l:
     ...:     print k
     ...:     
1
2
3
4

In [359]: for index1,index2 in zip(l,t):
     ...:     print index1,index2
     ...:     
     ...:     
1 t
2 e
3 s
4 t


    Functions
        index
        count
        
Functions

Lets look at few of the functions of the tuples.
index

index is used to return first index of the value.

In [107]: b = ('jan','feb','mar','apr')

In [108]: b.index('mar')
Out[108]: 2

count

count is used to find the occurance of the value in a tuple.

In [114]: b
Out[114]: ('jan', 'feb', 'mar', 'apr', 'jan', 'may')

In [112]: b.count('jan')
Out[112]: 2

In [113]: b.count('feb')
Out[113]: 1



Dictionaries
A dictionary is like a list, but more general. In a list, the indices have to be integers; in a dictionary they can be (almost) any type.

we can think of a dictionary as a mapping between a set of indices (which are called keys) and a set of values. Each key maps to a value. The association of a key and a value is called a key-value pair or sometimes an item.

we can use the {} for dictionary .

example:

new = {}

Lets take a quick example on understanding the dictionary variables.

Examples:

In [2]: a = dict()

In [3]: type(a)
Out[3]: dict

In [4]: print a
{}

Example on a set of dictionary variables.

In [5]: a['one'] = '1'

In [6]: print a
{'one': '1'}

In [7]: a = {'one':'1','two':'2','three':'3','four':'4'}

In [8]: a
Out[8]: {'four': '4', 'one': '1', 'three': '3', 'two': '2'}

The order of the key-value pair is not always the same. If we type on different systems we might find different output.

Example

In [7]: a = {'one':'1','two':'2','three':'3','four':'4'}

In [8]: a
Out[8]: {'four': '4', 'one': '1', 'three': '3', 'two': '2'}

Now lets take some examples on how to access each and every value of a dictionary element.

example

In [9]: a
Out[9]: {'four': '4', 'one': '1', 'three': '3', 'two': '2'}

In [10]: a['four']
Out[10]: '4'

If you notice , on giving the key value we are getting the value for the array. But if we lookup for a key , which is not there in the dictionary/hashes we get the below error.

In [11]: a['five']
---------------------------------------------------------------------------
KeyError                                  Traceback (most recent call last)
<ipython-input-11-bb0034013dcf> in <module>()
----> 1 a['five']

KeyError: 'five'



    Dictionary
        in operator
        del operator
        
in operator

we can impletement the in operator on the dictionaries. we can verify if a key is present in the hash or not.

example

In [15]: a
Out[15]: {'four': '4', 'one': '1', 'three': '3', 'two': '2'}

In [16]: 'four' in a
Out[16]: True

In [17]: 'four' in a
Out[17]: True

In [18]: '1' in a
Out[18]: False

In [19]: '3' in a
Out[19]: False

If you notice, we are getting ‘True’ if not its ‘False’.
del operator

we can delete the elements in the dictionary.

In [55]: del a['four']

In [56]: a
Out[56]: {'one': '1', 'three': '3', 'two': '2'}


    Looping in dictionaries
    
Looping in dictionaries

you can use looping to parse via the variable of the dictionary. Lets go via a quick example to understand the same.

Example:

In [21]: a
Out[21]: {'four': '4', 'one': '1', 'three': '3', 'two': '2'}

Output:

In [23]: for i in a:
print i,a[i]
 ....:
four 4
three 3
two 2
one 1

we can also grab the values in the following way.

In [25]: a
Out[25]: {'four': '4', 'one': '1', 'three': '3', 'two': '2'}

In [26]: for k,v in a.items():
 ....:     print(k,v)
 ....:
('four', '4')
('three', '3')
('two', '2')
('one', '1')


    Lookups
    
Lookups

we can always grab the value of a dictionary the moment we get the keys.

value = v[k]

similarly, if we want to get key by passing on the values its called as the reverse lookup.

    Dictionaries and Tuples
    Functions
        keys
            viewkeys
            iterkeys
        values
            viewvalues
            itervalues
        items
            iteritems
        clear
    Exercises
-----------------    TASK FOR TODAY---------------------------

    Perform a list assignment using the Python command line to assign the first 4 prime numbers 2,3,5 and 7 to a list called primes. Hint: you will need to use square brackets [] to assemble the list.

  1.  Append the 5th prime number (11) to this list using the append() method. Print out the primes list
   2.. Create another list called: b of the 6th to the 8th prime numbers ( which are 13, 17 and 19). Add b to the end of primes to create a new list c (adding primes and b together using the + operator). Write what happens when you print c.
  3.  Append the list b to the end of the list primes (using the append() method ). (Explanation: a list item can itself be a list in Python).
  4.  Print the value primes[5][2] . If you carried out the above exercises correctly this should print the value 19. Experiment with other indexes so that you print 17 or 13 in a similar way.

  5.  Assign a list to a reference a , containing a regular sequence of 5 - 8 elements, such that if you knew the first 3 elements you would be able to predict the rest. E.G: [3,6,9,12,15,21,24] .

 6.   Using a slice operation assign 2 elements from the middle of your sequence: e.g. 12 and 15 to another list called c .
   7. Take a backup of your list a in b by assigning: b=a You might need to copy the backup in b back to a if you screw a up.
  8.  Using the del operator twice on indexed elements of list a, remove the 2 elements from the middle of a that you assigned into the list c. E.G if you had (blindly) used the above values your list might now look like: [3,6,9,18,21,24].
   9 Using a slice assignment operation, restore the list a to its original sequence by inserting list c into the middle of list a.

   10. Create a list made up of all the following 4 lower-case words, 1 word per list item: [“the”,”dead”,”parrot”,”sketch”] . Assign this list to a reference called: parrot . Write and save (as for1.py ) a short program using a for loop, which prints out each word in turn, with the first letter capitalised, together with the length of each word. Your output should look like this:

The 3
Dead 4
Parrot 6
Sketch 6

Hint: you can split a string into 2 substrings using 2 slices. You can get a capitalised copy of one sub string using the .upper() method which is available if you import string . You can rejoin 2 substrings together again using the + operator.

    Save a copy of this program as for2.py. Edit it so that each time you go through the list and print a word you print one more uppercase letter than the previous word. Your output should look like this:

The
DEad
PARrot
SKETch

To do this you will need an integer reference which counts the number of times you have been through the loop.

11. Write a program which will find all such numbers which are divisible by 7 but are not a multiple of 5,between 2000 and 3200 (both included). The numbers obtained should be printed in a comma-separated sequence on a single line.

Hints: Consider use range(#begin, #end) method

12 .With a given integral number n, write a program to generate a dictionary that contains (i, i*i) such that is an integral number between 1 and n (both included). and then the program should print the dictionary. Suppose the following input is supplied to the program: 8 Then, the output should be: {1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49, 8: 64}

Hints: In case of input data being supplied to the question, it should be assumed to be a console input. Consider use dict()

13 . Write a program which accepts a sequence of comma-separated numbers from console and generate a list and a tuple which contains every number. Suppose the following input is supplied to the program: 34,67,55,33,12,98 Then, the output should be: [‘34’, ‘67’, ‘55’, ‘33’, ‘12’, ‘98’] (‘34’, ‘67’, ‘55’, ‘33’, ‘12’, ‘98’)

Hints: In case of input data being supplied to the question, it should be assumed to be a console input. tuple() method can convert list to tuple
